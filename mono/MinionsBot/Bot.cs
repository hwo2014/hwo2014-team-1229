using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class Bot : BotReceiver, IBot
    {
        #region Static Fields

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static readonly Turbo TurboMessage = new Turbo();

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static readonly Ping PingMessage = new Ping();

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static readonly SwitchLaneLeft SwitchLaneLeftMessage = new SwitchLaneLeft();

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static readonly SwitchLaneRight SwitchLaneRightMessage = new SwitchLaneRight();

        private const int TimeStep = 1;

        #endregion

        #region Fields

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly StreamWriter m_Writer;

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly StreamReader m_Reader;

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly BotMemory m_BotMemory;

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly BotProperties m_BotProperties;

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly BotPhysics m_BotPhysics;

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly BotMessageDistributor m_BotMessageDistributor;

        private TrackManager trackManager;
        private CarPosition lastTickCarPos;
        private DateTime timeOfLastCrash = DateTime.MinValue;

        #endregion

        #region Constructor

        public Bot([NotNull] StreamReader reader, [NotNull] StreamWriter writer, [NotNull] Join join)
        {
            Bot = this;
            Name = join.Name;
            Key = join.Key;

            m_BotMemory = new BotMemory(this);
            m_BotProperties = new BotProperties(this);
            m_BotPhysics = new BotPhysics(this);
            m_Writer = writer;
            m_Reader = reader;

            m_BotMessageDistributor = new BotMessageDistributor(this);
            m_BotMessageDistributor.AddReceiver(m_BotProperties);
            m_BotMessageDistributor.AddReceiver(m_BotMemory);

            Send(join);
        }

        #endregion

        #region Implementation of IBotProvider

        IBot IBotProvider.Bot
        {
            get { return this; }
        }

        #endregion

        #region Implementation of IBot

        public IBotProperties Properties
        {
            get { return m_BotProperties; }
        }

        public IBotMemory Memory
        {
            get { return m_BotMemory; }
        }

        public IBotPhysics Physics
        {
            get { return m_BotPhysics; }
        }

        public ITrack Track { get; private set; }

        public IBotMessageDistributor MessageDistributor
        {
            get { return m_BotMessageDistributor; }
        }

        public string Key { get; private set; }

        public string Name { get; private set; }

        public void Throttle(double throttle, int gameTick)
        {
            Memory.AddThrottle(throttle, gameTick);

            Log("GameTick = {0:000}, throttle = {1:0.00}", gameTick, throttle);
            Send(new Throttle(throttle));
        }

        public void ActivateTurbo(int gameTick)
        {
            Log("GameTick = {0:000}, activating turbo", gameTick);
            Send(TurboMessage);
        }

        public void SendPing()
        {
            Send(PingMessage);
        }

        public void SwitchLeft(int gameTick)
        {
            Log("GameTick = {0:000}, switch left", gameTick);
            Send(SwitchLaneLeftMessage);
        }

        public void SwitchRight(int gameTick)
        {
            Log("GameTick = {0:000}, switch right", gameTick);
            Send(SwitchLaneRightMessage);
        }

        public bool EqualId(Id id)
        {
            return !ReferenceEquals(id, null) && (Name == id.Name || Key == id.Key);
        }

        #endregion

        #region Methods


        [StringFormatMethod("format")]
        private static void Log([NotNull] string format, params object[] args)
        {
            Console.Write(format, args);
            Console.WriteLine();
        }

        private static void Log([NotNull] string message)
        {
            Console.WriteLine(message);
        }

        private static T JsonConvert<T>([NotNull] string line)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(line);
        }

        private static T Convert<T>([CanBeNull] SendMessageWrapper sendMessage, [NotNull] string line)
        {
            if (typeof (T).IsSubclassOf(typeof (ReceiveMessage)))
            {
                return JsonConvert<T>(line);
            }

            return ReferenceEquals(sendMessage, null) ? default(T) : JsonConvert<T>(System.Convert.ToString(sendMessage.Data));
        }

        public void GameLoop()
        {
            string line;

            while ((line = m_Reader.ReadLine()) != null)
            {
                var msg = JsonConvert<SendMessageWrapper>(line);
                switch (msg.MsgType)
                {
                    case CarPositions.MsgId:
                        MessageDistributor.AddEventCarPositions(Convert<CarPositions>(msg, line));
                        break;

                    case Join.MsgId:
                        MessageDistributor.AddEventJoin();
                        break;

                    case RaceInit.MsgId:
                        MessageDistributor.AddEventGameInit(Convert<RaceInit>(msg, line));
                        break;

                    case "gameEnd":
                        MessageDistributor.AddEventGameEnd(Convert<RaceResult>(msg, line));
                        break;

                    case "gameStart":
                        MessageDistributor.AddEventGameStart();
                        break;

                    case "yourCar":
                        MessageDistributor.AddEventYourCar(Convert<Id>(msg, line));
                        break;

                    case Crash.MsgId:
                        MessageDistributor.AddEventCrash(Convert<Crash>(msg, line));
                        break;

                    case TournamentEnd.MsgId:
                        MessageDistributor.AddEventTournamentEnd(Convert<TournamentEnd>(msg, line));
                        break;

                    case Spawn.MsgId:
                        MessageDistributor.AddEventSpawn(Convert<Spawn>(msg, line));
                        break;

                    case LapFinished.MsgId:
                        MessageDistributor.AddEventLapFinished(Convert<LapFinished>(msg, line));
                        break;

                    case Disqualified.MsgId:
                        MessageDistributor.AddEventDisqualified(Convert<Disqualified>(msg, line));
                        break;

                    case Finish.MsgId:
                        MessageDistributor.AddEventFinish(Convert<Finish>(msg, line));
                        break;

                    case TurboAvailable.MsgId:
                        MessageDistributor.AddEventTurboAvailable(Convert<TurboAvailable>(msg, line));
                        break;

                    case TurboStart.MsgId:
                        MessageDistributor.AddEventTurboStart(Convert<TurboStart>(msg, line));
                        break;

                    case TurboEnd.MsgId:
                        MessageDistributor.AddEventTurboEnd(Convert<TurboEnd>(msg, line));
                        break;
                        

                    default:
                        Log("answering ping to msg: " + msg.MsgType);
                        SendPing();
                        break;
                }
            }
        }



        private void Send([CanBeNull] SendMessage message)
        {
            if (!ReferenceEquals(message, null))
            {
                m_Writer.WriteLine(message.ToJson());
            }
        }

        #endregion

        #region Overrides of BotReceiver

        protected override void OnTurboAvailable(TurboData turbo)
        {
            SendPing();
        }

        protected override void OnSpawn(Spawn spawn)
        {
            if (EqualId(spawn.CarId))
            {
                Log("spawned!");
            }

            SendPing();
        }

        protected override void OnFinish(Finish finish)
        {
            SendPing();
        }

        protected override void OnDisqualified(Disqualified dnf)
        {
            if (EqualId(dnf.Data.CarId))
            {
                Log("disqualified!");
            }

            SendPing();
        }

        protected override void OnCrash(Crash crash)
        {
            if (EqualId(crash.CarId))
            {
                Log("crashed!");
            }

            SendPing();
        }

        public void OnTournamentEnd(TournamentEnd tournamentEnd)
        {
            SendPing();
        }

        protected override void OnLapFinished(LapFinished lapFinished)
        {
            if (EqualId(lapFinished.Data.CarId))
            {
                Log("Lap finished! tick: " + Properties.GameTick);
            }


            SendPing();
        }

        protected override void OnGameInit(Race race)
        {
            Log("Race init, created new track manager." + Properties.GameTick);
            trackManager = new TrackManager(race);
            Track = trackManager;
            SendPing();
        }

        protected override void OnGameStart()
        {
            SendPing();
        }

        protected override void OnGameEnd(RaceResult raceResult)
        {
            SendPing();
        }

        protected override void OnYourCar(Id yourCar)
        {
            SendPing();
        }

        protected override void OnJoin()
        {
            SendPing();
        }

        private static void GenerateSimulationSequence([NotNull] IList<double> throttles, double start, double end)
        {
            for (var index = 0; index < throttles.Count; ++index)
            {
                throttles[index] = start + index*(end - start)/throttles.Count;
            }
        }

        protected override void OnCarPositions(CarPositions carPositions)
        {
            var ourPosition = carPositions.Positions.First(x => x.Id.Name == Name);
            var currentGameTick = Properties.GameTick;
            Physics.UpdateIntegration(currentGameTick, TimeStep); // update physic engine

            var maxThrottle = 1.0;
            double throttle = 1.0;
            CarPhysics currentCarPhysics;
            if ((Physics.TryGetCarPhysics(currentGameTick, out currentCarPhysics) && currentCarPhysics.Acceleration < BotPhysics.Epsilon) || Properties.TicksSinceStartOrSpawn < 10)
            {
                Physics.LearnThrottleFactor(currentGameTick, throttle);
            }
            else // only go here if enough samples have been collected
            {
                var lineLength = Track.GetLineLength(ourPosition.PiecePosition.PieceIndex);
                if (lineLength >= 4)
                {
                    maxThrottle = 1.0;
                }
                else if (lineLength >= 3)
                {
                    maxThrottle = 0.9;
                }
                else if (lineLength >= 2)
                {
                    maxThrottle = 0.8;
                }
             
                var currentThrottle = Memory.Throttles[Memory.Throttles.Keys[Memory.Throttles.Count - 1]];

                var throttles = new double[5]; // num simulation steps
                GenerateSimulationSequence(throttles, currentThrottle, 1.0);

                // simulate ...
                var simulationResult = Physics.Simulate(currentGameTick, TimeStep, throttles);

                var lastPosition = simulationResult.Last().Value;
                var angleAbs = Math.Abs(lastPosition.GetAngle());
                throttle = Math.Exp(-4.0*angleAbs);

                if (!ReferenceEquals(currentCarPhysics, null))
                {
                    throttle = (currentCarPhysics.Throttle + throttle) / 2.0;
                }

                for (var distance = 0; distance < 3; ++distance)
                {
                    var curveAngle = Track.GetCurveAngle(ourPosition.PiecePosition.PieceIndex + distance);
                    var absCurveAngle = Math.Abs(curveAngle);
                    if (absCurveAngle > BotPhysics.Epsilon)
                    {
                        var radians = absCurveAngle/180.0*Math.PI;
                        maxThrottle = 1.0/(radians + 1.0);
                        break;
                    }
                }
            }

            throttle = Math.Max(0.25, Math.Min(maxThrottle, throttle)); // throttle in range [0.25, 1]

            Physics.IntegrateAdd(currentGameTick, TimeStep, throttle);
            if (Properties.IsCrashed)
            {
                SendPing();
            }
            else
            {
                // check whether path is blocked by another car
                var ourPiecePosition = ourPosition.PiecePosition;
                if (ourPiecePosition.Lane.StartLaneIndex == ourPiecePosition.Lane.EndLaneIndex) // not switched yet
                {
                    var piece = Track.Pieces[ourPiecePosition.PieceIndex];
                    if (piece.Switch.HasValue && piece.Switch.Value)
                    {
                        foreach (var carPosition in carPositions.Positions)
                        {
                            if (!ReferenceEquals(ourPosition, carPosition))
                            {
                                var piecePosition = carPosition.PiecePosition;
                                if (ourPiecePosition.Lane.StartLaneIndex == piecePosition.Lane.StartLaneIndex)
                                {
                                    var d = Track.GetPieceDistance(ourPiecePosition.PieceIndex, piecePosition.PieceIndex);
                                    if (d >= 0 && d <= 1)
                                    {
                                        if (Track.Lanes[ourPiecePosition.Lane.StartLaneIndex].DistanceFromCenter > 0)
                                        {
                                            SwitchLeft(currentGameTick);
                                        }
                                        else
                                        {
                                            SwitchRight(currentGameTick);
                                        }
                                        return;
                                    }
                                }

                            }
                        }
                    }
                }

                Throttle(throttle, currentGameTick);
            }

            //Log("index: " + ourPosition.PiecePosition.PieceIndex + " inPos: " + ourPosition.PiecePosition.InPieceDistance);
            ////if (trackManager.Track[ourPosition.piecePosition.pieceIndex].angle != null)
            ////{
            ////    Log("length: " + trackManager.Track[ourPosition.piecePosition.pieceIndex].length); //0 if bend
            ////    Log("index: " + ourPosition.piecePosition.pieceIndex);
            ////    Log("inPos: " + ourPosition.piecePosition.inPieceDistance);
            ////    Log("angle: " + ourPosition.angle);
            ////}

            //if (lastTickCarPos != null)
            //{
            //    if(lastTickCarPos.PiecePosition.PieceIndex == ourPosition.PiecePosition.PieceIndex &&
            //        lastTickCarPos.PiecePosition.InPieceDistance == ourPosition.PiecePosition.InPieceDistance) //TODO: maybe compare gametick?
            //    {
            //        Log("crashed " + (DateTime.Now - timeOfLastCrash).TotalSeconds);
            //        //we did not move in the last tick so report crash
            //        //TODO detect how long the crash penalty is
            //        TimeSpan fiveSec = new TimeSpan(0, 0, 5);

            //        //report crashes only every 5 sec
            //        if (DateTime.Now - timeOfLastCrash >= fiveSec)
            //        {
            //            timeOfLastCrash = DateTime.Now;
            //            Log("==============>>>>>>  reporting crash at: " + ourPosition.PiecePosition.PieceIndex);
            //            trackManager.ReportCrash(ourPosition.PiecePosition.PieceIndex, ourPosition.PiecePosition.InPieceDistance);
            //        }
            //    }
            //}





            ////double throttle = random.NextDouble() + 0.3
            //double throttle = trackManager.GetThrottleForTrack(ourPosition.PiecePosition.PieceIndex, ourPosition.PiecePosition.InPieceDistance);

            //lastTickCarPos = ourPosition;
            //Throttle(throttle, Properties.GameTick);
        }

        #endregion

       
    }
}
