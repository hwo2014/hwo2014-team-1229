﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class BotMemory : BotReceiver, IBotMemory
    {
        #region Fields

        private readonly Dictionary<string, IList<Spawn>> m_SpawnsLookup = new Dictionary<string, IList<Spawn>>();
        private readonly Dictionary<string, Finish> m_FinishesLookup = new Dictionary<string, Finish>();
        private readonly Dictionary<string, Disqualified> m_DisqualificationsLookup = new Dictionary<string, Disqualified>();
        private readonly Dictionary<string, IList<Crash>> m_CrashesLookup = new Dictionary<string, IList<Crash>>();
        private readonly Dictionary<string, IList<LapFinished>> m_LapsLookup = new Dictionary<string, IList<LapFinished>>();
        private readonly List<TurboData> m_Turbos = new List<TurboData>();
        private readonly List<IReadOnlyList<CarPosition>> m_CarPositions = new List<IReadOnlyList<CarPosition>>();
        private readonly Dictionary<string, SortedList<int, CarPosition>> m_CarPositionsLookup = new Dictionary<string, SortedList<int, CarPosition>>();
        private readonly SortedList<int, double> m_ThrottleLookup = new SortedList<int, double>(); 

        #endregion

        #region Constructor

        public BotMemory([NotNull] IBot bot)
            : base(bot)
        {
            Reset();
        }

        #endregion

        #region Static Methods

        private static void AddToLookup<T>([NotNull] IDictionary<string, T> lookup, [CanBeNull] Id key, T value)
        {
            if (!ReferenceEquals(key, null) && !ReferenceEquals(key.Name, null))
            {
                if (!lookup.ContainsKey(key.Name))
                {
                    lookup.Add(key.Name, value);
                }
                else
                {
                    lookup[key.Name] = value;
                }
            }
        }

        private static void AddToLookup<T>([NotNull] IDictionary<string, IList<T>> lookup, [CanBeNull] Id key, T value)
        {
            if (!ReferenceEquals(key, null) && !ReferenceEquals(key.Name, null))
            {
                IList<T> list;
                if (!lookup.TryGetValue(key.Name, out list))
                {
                    lookup.Add(key.Name, list = new List<T>());
                }

                list.Add(value);
            }
        }

        #endregion

        #region Overrides of BotReceiver

        protected override void OnTurboAvailable(TurboData turbo)
        {
            m_Turbos.Add(turbo);
        }

        protected override void OnSpawn(Spawn spawn)
        {
            AddToLookup(m_SpawnsLookup, spawn.CarId, spawn);
        }

        protected override void OnFinish(Finish finish)
        {
            AddToLookup(m_FinishesLookup, finish.CarId, finish);
        }

        protected override void OnDisqualified(Disqualified dnf)
        {
            if (!ReferenceEquals(dnf.Data, null))
            {
                AddToLookup(m_DisqualificationsLookup, dnf.Data.CarId, dnf);
            }
        }

        protected override void OnCrash(Crash crash)
        {
            AddToLookup(m_CrashesLookup, crash.CarId, crash);
        }

        protected override void OnLapFinished(LapFinished lapFinished)
        {
            if (!ReferenceEquals(lapFinished.Data, null))
            {
                AddToLookup(m_LapsLookup, lapFinished.Data.CarId, lapFinished);
            }
        }

        protected override void OnGameInit(Race race)
        {
            Race = race;
        }

        protected override void OnGameEnd(RaceResult raceResult)
        {
            RaceResult = raceResult;
        }

        protected override void OnCarPositions(CarPositions carPositions)
        {
            m_CarPositions.Add(carPositions.Positions);

            var gameTick = Bot.Properties.GameTick;
            foreach (var carPosition in carPositions.Positions)
            {
                SortedList<int, CarPosition> list;
                if (!m_CarPositionsLookup.TryGetValue(carPosition.Id.Name, out list))
                {
                    m_CarPositionsLookup.Add(carPosition.Id.Name, list = new SortedList<int, CarPosition>());
                }

                if (!list.ContainsKey(gameTick))
                {
                    list.Add(gameTick, carPosition);
                }
                else
                {
                    list[gameTick] = carPosition;
                }
            }
        }

        #endregion

        #region Implementation of IBotMemory

        public IReadOnlyDictionary<string, IList<Spawn>> Spawns
        {
            get { return m_SpawnsLookup; }
        }

        public IReadOnlyDictionary<string, Finish> Finishes
        {
            get { return m_FinishesLookup; }
        }

        public IReadOnlyDictionary<string, Disqualified> Disqualifications
        {
            get { return m_DisqualificationsLookup; }
        }

        public IReadOnlyDictionary<string, IList<Crash>> Crashes
        {
            get { return m_CrashesLookup; }
        }

        public IReadOnlyDictionary<string, IList<LapFinished>> Laps
        {
            get { return m_LapsLookup; }
        }

        public IReadOnlyList<TurboData> Turbos
        {
            get { return m_Turbos; }
        }

        public IReadOnlyList<IReadOnlyList<CarPosition>> AllCarPositions
        {
            get { return m_CarPositions; }
        }

        public IReadOnlyDictionary<string, SortedList<int, CarPosition>> CarPositions
        {
            get { return m_CarPositionsLookup; }
        }

        public SortedList<int, double> Throttles
        {
            get { return m_ThrottleLookup; }
        }

        public Race Race { get; private set; }

        public RaceResult RaceResult { get; private set; }

        public void AddThrottle(double throttle, int gameTick)
        {
            if (!m_ThrottleLookup.ContainsKey(gameTick))
            {
                m_ThrottleLookup.Add(gameTick, throttle);
            }
            else
            {
                m_ThrottleLookup[gameTick] = throttle;
            }
        }

        public void Reset()
        {
            m_SpawnsLookup.Clear();
            m_FinishesLookup.Clear();
            m_Turbos.Clear();
            m_DisqualificationsLookup.Clear();
            m_LapsLookup.Clear();
            m_CrashesLookup.Clear();
            m_CarPositions.Clear();
            m_CarPositionsLookup.Clear();
            m_ThrottleLookup.Clear();

            Race = null;
            RaceResult = null;
        }

        #endregion
    }
}
