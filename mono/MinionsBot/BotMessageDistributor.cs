﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class BotMessageDistributor : BotProvider, IBotMessageDistributor
    {
        #region Fields

        [NotNull]
        private readonly List<IBotReceiver> m_Receivers = new List<IBotReceiver>();

        #endregion

        #region Constructor

        public BotMessageDistributor([NotNull] IBot bot)
            : base(bot)
        {
        }

        #endregion

        #region Implementation of IBotMessageDistributor

        public IReadOnlyList<IBotReceiver> Receivers
        {
            get { return m_Receivers; }
        }

        public void AddReceiver(IBotReceiver receiver)
        {
            if (!ReferenceEquals(receiver, null))
            {
                m_Receivers.Add(receiver);
            }
        }

        public bool RemoveReceiver(IBotReceiver receiver)
        {
            return !ReferenceEquals(receiver, null) && m_Receivers.Remove(receiver);
        }

        public void Clear()
        {
            m_Receivers.Clear();
        }

        public void AddEventTurboAvailable(TurboAvailable data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventTurboAvailable(data);
            }

            Bot.EventTurboAvailable(data);
        }

        public void AddEventFinish(Finish data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventFinish(data);
            }

            Bot.EventFinish(data);
        }

        public void AddEventDisqualified(Disqualified data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventDisqualified(data);
            }

            Bot.EventDisqualified(data);
        }

        public void AddEventLapFinished(LapFinished data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventLapFinished(data);
            }

            Bot.EventLapFinished(data);
        }

        public void AddEventSpawn(Spawn data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventSpawn(data);
            }

            Bot.EventSpawn(data);
        }

        public void AddEventTournamentEnd(TournamentEnd data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventTournamentEnd(data);
            }

            Bot.EventTournamentEnd(data);
        }

        public void AddEventCrash(Crash data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventCrash(data);
            }

            Bot.EventCrash(data);
        }

        public void AddEventYourCar(Id data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventYourCar(data);
            }

            Bot.EventYourCar(data);
        }

        public void AddEventGameStart()
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventGameStart();
            }

            Bot.EventGameStart();
        }

        public void AddEventGameEnd(RaceResult data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventGameEnd(data);
            }

            Bot.EventGameEnd(data);
        }

        public void AddEventGameInit(RaceInit data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventGameInit(data);
            }

            Bot.EventGameInit(data);
        }

        public void AddEventJoin()
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventJoin();
            }

            Bot.EventJoin();
        }

        public void AddEventCarPositions(CarPositions data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventCarPositions(data);
            }

            Bot.EventCarPositions(data);
        }

        public void AddEventTurboStart(TurboStart data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventTurboStart(data);
            }

            Bot.EventTurboStart(data);
        }

        public void AddEventTurboEnd(TurboEnd data)
        {
            foreach (var botReceiver in Receivers)
            {
                botReceiver.EventTurboEnd(data);
            }

            Bot.EventTurboEnd(data);
        }

        #endregion
    }
}
