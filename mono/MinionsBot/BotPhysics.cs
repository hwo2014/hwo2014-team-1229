﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class BotPhysics : BotProvider, IBotPhysics
    {
        #region Constants

        public const double Epsilon = 0.00001;
        private const int InvalidTime = -1;

        #endregion

        #region Fields

        [NotNull]
        private readonly SortedList<int, CarPhysics> m_CarPhysics = new SortedList<int, CarPhysics>();

        private double m_ThrottleFactor = 5.0;

        #endregion

        #region Constructor

        public BotPhysics([NotNull] IBot bot)
            : this(bot, null)
        {
        }

        public BotPhysics([NotNull] IBot bot, [CanBeNull] string botName)
            : base(bot)
        {
            Name = string.IsNullOrEmpty(botName) ? bot.Name : botName;
        }

        #endregion

        #region Methods

        private double ComputePosition([NotNull] CarPhysics car1, [NotNull] CarPhysics car2)
        {
            // TODO correct implementation
            if (ReferenceEquals(car2.CarPosition, null))
            {
                return car1.CarPosition.PiecePosition.InPieceDistance;
            }

            if (car1.CarPosition.PiecePosition.PieceIndex == car2.CarPosition.PiecePosition.PieceIndex)
            {
                return car2.Position + (car1.CarPosition.PiecePosition.InPieceDistance - car2.CarPosition.PiecePosition.InPieceDistance);
            }

            return car2.Position + Bot.Track.MeasureRemainingLength(car2.CarPosition.PiecePosition) + car1.CarPosition.PiecePosition.InPieceDistance;
        }

        private void UpdateIntegration([NotNull] SortedList<int, CarPhysics> carPhysics, [NotNull] SortedList<int, CarPosition> carPositions, int gameTick, int timeStep)
        {
            if (carPhysics.Count >= 2)
            {
                UpdateIntegration(GetValue(carPhysics, gameTick), GetValue(carPhysics, gameTick, -timeStep), GetValue(carPositions, gameTick), timeStep);
            }
            else if (carPhysics.Count >= 1)
            {
                UpdateIntegration(GetValue(carPhysics, gameTick), CarPhysics.Empty, GetValue(carPositions, gameTick), timeStep);
            }
        }

        private void UpdateIntegration([CanBeNull] CarPhysics car1, [CanBeNull] CarPhysics car2, [CanBeNull] CarPosition carPosition, int timeStep)
        {
            if (!ReferenceEquals(car1, null) && !ReferenceEquals(carPosition, null))
            {
                double throttle;
                if (Bot.Memory.Throttles.TryGetValue(car1.GameTick, out throttle))
                {
                    car1.Throttle = throttle;
                }

                car1.CarPosition = carPosition;

                if (ReferenceEquals(car2, null))
                {
                    car2 = CarPhysics.Empty;
                }

                car1.Position = ComputePosition(car1, car2);
                car1.Velocity = (car1.Position - car2.Position)/timeStep;
                car1.Acceleration = (car1.Velocity - car2.Velocity)/timeStep;
                car1.ThrottleAcceleration = (car1.Acceleration - car2.Acceleration)/timeStep;

                car1.Angle = carPosition.Angle;
                car1.AngularVelocity = (car1.Angle - car2.Angle)/timeStep;
                car1.AngularAcceleration = (car1.AngularVelocity - car2.AngularVelocity)/timeStep;
            }
        }

        [NotNull]
        private CarPhysics Integrate([CanBeNull] CarPhysics car1, int timeStep, double throttle)
        {
            var newCarPhysics = new CarPhysics();

            if (!ReferenceEquals(car1, null))
            {
                var throttlePower = timeStep*(throttle/m_ThrottleFactor);
                newCarPhysics.EstimatedThrottleAcceleration = car1.GetThrottleAcceleration() + throttlePower;

                newCarPhysics.EstimatedAcceleration = car1.GetAcceleration() + timeStep*newCarPhysics.EstimatedThrottleAcceleration;
                newCarPhysics.EstimatedVelocity = car1.GetVelocity() + timeStep*newCarPhysics.EstimatedAcceleration;
                newCarPhysics.EstimatedPosition = car1.GetPosition() + timeStep*newCarPhysics.EstimatedVelocity - throttlePower;

                newCarPhysics.EstimatedAngularAcceleration = car1.GetAngularAcceleration();
                newCarPhysics.EstimatedAngularVelocity = car1.GetAngularVelocity() + timeStep*newCarPhysics.EstimatedAngularAcceleration;
                newCarPhysics.EstimatedAngle = car1.GetAngle() + timeStep*newCarPhysics.EstimatedAngularVelocity;
                
                newCarPhysics.GameTick = car1.GameTick + timeStep;
                newCarPhysics.Throttle = throttle;
            }

            return newCarPhysics;
        }

        private static int SearchTime<T>([NotNull] SortedList<int, T> carPhysics, int gameTick)
        {
            var keys = carPhysics.Keys;
            for (var index = keys.Count - 1; index >= 0; --index)
            {
                var time = keys[index];
                if (time <= gameTick)
                {
                    return time;
                }
            }

            return InvalidTime;
        }

        [CanBeNull]
        private static T GetValue<T>([NotNull] SortedList<int, T> values, [CanBeNull] SortedList<int, T> additionalValues, int gameTick, int timeStep = 0) where T : class, IGameTickProvider
        {
            var gameTime = gameTick + timeStep;

            int time;
            if (!ReferenceEquals(additionalValues, null))
            {
                time = SearchTime(additionalValues, gameTime);
                if (time != InvalidTime)
                {
                    return additionalValues[time];
                }
            }

            time = SearchTime(values, gameTime);
            return time != InvalidTime ? values[time] : null;
        }

        [CanBeNull]
        private static T GetValue<T>([NotNull] SortedList<int, T> carPhysics, int gameTick, int timeStep = 0) where T : class
        {
            var time = SearchTime(carPhysics, gameTick + timeStep);
            return time == InvalidTime ? null : carPhysics[time];
        }

        [NotNull]
        private CarPhysics Integrate([CanBeNull] SortedList<int, CarPhysics> additionalPhysics,
                                            int gameTick, int timeStep, double throttle)
        {
            if (m_CarPhysics.Count >= 1)
            {
                return Integrate(GetValue(m_CarPhysics, additionalPhysics, gameTick), timeStep, throttle);
            }

            return new CarPhysics
                   {
                       GameTick = gameTick + timeStep,
                       Throttle = throttle
                   };
        }

        #endregion

        #region Implementation of IBotPhysics

        public string Name { get; private set; }

        public int Count
        {
            get { return m_CarPhysics.Count; }
        }

        public void Reset()
        {
            m_CarPhysics.Clear();
        }

        public void LearnThrottleFactor(int gameTick, double throttle)
        {
            foreach (var physic in m_CarPhysics)
            {
                if (physic.Value.ThrottleAcceleration > Epsilon)
                {
                    m_ThrottleFactor = throttle / physic.Value.ThrottleAcceleration;
                    break;
                }
            }
        }

        public bool UpdateIntegration(int gameTick, int timeStep)
        {
            SortedList<int, CarPosition> carPositions;
            if (ReferenceEquals(Bot.Track, null) || !Bot.Memory.CarPositions.TryGetValue(Name, out carPositions) || carPositions.Count <= 0)
            {
                return false;
            }

            UpdateIntegration(m_CarPhysics, carPositions, gameTick, timeStep);
            return true;
        }

        public CarPhysics Integrate(int gameTick, int timeStep, double throttle)
        {
            return Integrate(null, gameTick, timeStep, throttle);
        }

        public SortedList<int, CarPhysics> Simulate(int gameTick, int timeStep, double[] throttles)
        {
            if (ReferenceEquals(throttles, null))
            {
                throw new ArgumentNullException("throttles");
            }

            var simulatedValues = new SortedList<int, CarPhysics>(throttles.Length);
            foreach (var throttle in throttles)
            {
                var nextStep = Integrate(simulatedValues, gameTick, timeStep, throttle);
                simulatedValues.Add(nextStep.GameTick, nextStep);
                gameTick = nextStep.GameTick;
            }
            return simulatedValues;
        }

        public CarPhysics IntegrateAdd(int gameTick, int timeStep, double throttle)
        {
            var newCarPhysics = Integrate(gameTick, timeStep, throttle);
            if (!m_CarPhysics.ContainsKey(newCarPhysics.GameTick))
            {
                m_CarPhysics.Add(newCarPhysics.GameTick, newCarPhysics);
            }
            else
            {
                m_CarPhysics[newCarPhysics.GameTick] = newCarPhysics;
            }

            return newCarPhysics;
        }

        public bool TryGetCarPhysics(int gameTick, int timeStep, out CarPhysics carPhysics)
        {
            return TryGetCarPhysics(gameTick + timeStep, out carPhysics);
        }

        public bool TryGetCarPhysics(int gameTick, out CarPhysics carPhysics)
        {
            return m_CarPhysics.TryGetValue(gameTick, out carPhysics);
        }

        #endregion
    }
}
