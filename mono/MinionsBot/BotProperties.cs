﻿using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class BotProperties : BotReceiver, IBotProperties
    {
        #region Constructor

        public BotProperties([NotNull] IBot bot)
            : base(bot)
        {
            Reset();
        }

        #endregion

        #region Implementation of IBotProperties

        public TurboData AvailableTurbo { get; private set; }

        public bool IsTurboAvailable
        {
            get { return !ReferenceEquals(AvailableTurbo, null); }
        }

        public int GameTick { get; private set; }
        public int Lap { get; private set; }
        public int LastLapMilliseconds { get; private set; }
        public bool IsDisqualified { get; private set; }
        public bool IsRaceStarted { get; private set; }
        public bool IsRaceFinished { get; private set; }
        public bool IsTournamentEnded { get; private set; }
        public bool IsJoined { get; private set; }
        public bool IsCrashed { get; private set; }
        public bool IsGameEnded { get; private set; }
        public bool IsGameInitialized { get; private set; }
        public int TicksSinceStartOrSpawn { get; private set; }

        public void Reset()
        {
            AvailableTurbo = null;
            GameTick = 0;
            Lap = 0;
            LastLapMilliseconds = -1;
            IsDisqualified = false;
            IsRaceStarted = false;
            IsRaceFinished = false;
            IsJoined = false;
            IsCrashed = false;
            IsTournamentEnded = false;
            IsGameEnded = false;
            IsGameInitialized = false;
            TicksSinceStartOrSpawn = 0;
        }

        #endregion

        #region Overrides of BotReceiver

        protected override void OnTurboAvailable(TurboData turbo)
        {
            if (!IsCrashed)
            {
                AvailableTurbo = turbo;
            }
        }

        protected override void OnSpawn(Spawn spawn)
        {
            if (Bot.EqualId(spawn.CarId))
            {
                TicksSinceStartOrSpawn = 0;
                GameTick = spawn.GameTick;
                IsCrashed = false;
            }
        }

        protected override void OnFinish(Finish finish)
        {
            if (Bot.EqualId(finish.CarId))
            {
                GameTick = finish.GameTick;
                IsRaceFinished = true;
            }
        }

        protected override void OnDisqualified(Disqualified dnf)
        {
            if (!ReferenceEquals(dnf.Data, null) && Bot.EqualId(dnf.Data.CarId))
            {
                GameTick = dnf.GameTick;
                IsDisqualified = true;
            }
        }

        protected override void OnCrash(Crash crash)
        {
            if (Bot.EqualId(crash.CarId))
            {
                GameTick = crash.GameTick;
                IsCrashed = true;
            }
        }

        protected override void OnTournamentEnd()
        {
            IsTournamentEnded = true;
        }

        protected override void OnLapFinished(LapFinished lapFinished)
        {
            if (!ReferenceEquals(lapFinished.Data, null) && Bot.EqualId(lapFinished.Data.CarId))
            {
                GameTick = lapFinished.GameTick;
                ++Lap;

                LastLapMilliseconds = !ReferenceEquals(lapFinished.Data.LapTime, null)
                    ? lapFinished.Data.LapTime.Millis
                    : -1;
            }
        }

        protected override void OnGameInit(Race race)
        {
            IsGameInitialized = true;
        }

        protected override void OnGameStart()
        {
            GameTick = 0;
            TicksSinceStartOrSpawn = 0;
            IsRaceStarted = true;
        }

        protected override void OnGameEnd(RaceResult raceResult)
        {
            IsGameEnded = true;
        }

        protected override void OnJoin()
        {
            IsJoined = true;
        }

        protected override void OnCarPositions(CarPositions carPositions)
        {
            if (!IsCrashed)
            {
                ++TicksSinceStartOrSpawn;
            }

            GameTick = carPositions.GameTick;
        }

        #endregion
    }
}
