﻿using Minions.Interfaces;

namespace Minions
{
    public abstract class BotProvider : IBotProvider
    {
        #region Constructor

        protected BotProvider(IBot bot)
        {
            Bot = bot;
        }

        #endregion

        #region Implementation of IBotProvider

        public IBot Bot { get; protected set; }

        #endregion
    }
}
