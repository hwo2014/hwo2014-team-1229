﻿using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public abstract class BotReceiver : BotProvider, IBotReceiver
    {
        #region Constructor

        protected BotReceiver()
            : base(null)
        {
        }

        protected BotReceiver([NotNull] IBot bot)
            : base(bot)
        {
        }

        #endregion

        #region Methods

        protected virtual void OnTurboAvailable([NotNull] TurboData turbo)
        {
        }

        protected virtual void OnSpawn([NotNull] Spawn spawn)
        {
        }

        protected virtual void OnFinish([NotNull] Finish finish)
        {
        }

        protected virtual void OnDisqualified([NotNull] Disqualified dnf)
        {
        }

        protected virtual void OnCrash([NotNull] Crash crash)
        {
        }

        protected virtual void OnTournamentEnd()
        {
        }

        protected virtual void OnLapFinished([NotNull] LapFinished lapFinished)
        {
        }

        protected virtual void OnGameInit([NotNull] Race race)
        {
        }

        protected virtual void OnGameStart()
        {
        }

        protected virtual void OnGameEnd([NotNull] RaceResult raceResult)
        {
        }

        protected virtual void OnYourCar([NotNull] Id yourCar)
        {
        }

        protected virtual void OnJoin()
        {
        }

        protected virtual void OnCarPositions([NotNull] CarPositions carPositions)
        {
        }

        protected virtual void OnTurboStart([NotNull] TurboStart turboStart)
        {
        }

        protected virtual void OnTurboEnd([NotNull] TurboEnd turboEnd)
        {
        }

        #endregion

        #region Implementation of IBotReceiver

        void IBotReceiver.EventTurboAvailable(TurboAvailable turboAvailable)
        {
            if (!ReferenceEquals(turboAvailable, null) && !ReferenceEquals(turboAvailable.Turbo, null))
            {
                OnTurboAvailable(turboAvailable.Turbo);
            }
        }

        void IBotReceiver.EventSpawn(Spawn spawn)
        {
            if (!ReferenceEquals(spawn, null))
            {
                OnSpawn(spawn);
            }
        }

        void IBotReceiver.EventFinish(Finish finish)
        {
            if (!ReferenceEquals(finish, null))
            {
                OnFinish(finish);
            }
        }

        void IBotReceiver.EventDisqualified(Disqualified dnf)
        {
            if (!ReferenceEquals(dnf, null) && !ReferenceEquals(dnf.Data, null))
            {
                OnDisqualified(dnf);
            }
        }

        void IBotReceiver.EventCrash(Crash crash)
        {
            if (!ReferenceEquals(crash, null))
            {
                OnCrash(crash);
            }
        }

        void IBotReceiver.EventTournamentEnd(TournamentEnd tournamentEnd)
        {
            OnTournamentEnd();
        }

        void IBotReceiver.EventLapFinished(LapFinished lapFinished)
        {
            if (!ReferenceEquals(lapFinished, null))
            {
                OnLapFinished(lapFinished);
            }
        }

        void IBotReceiver.EventGameInit(RaceInit raceInit)
        {
            if (!ReferenceEquals(raceInit, null) && !ReferenceEquals(raceInit.Race, null))
            {
                OnGameInit(raceInit.Race);
            }
        }

        void IBotReceiver.EventGameStart()
        {
            OnGameStart();
        }

        void IBotReceiver.EventGameEnd(RaceResult raceResult)
        {
            if (!ReferenceEquals(raceResult, null))
            {
                OnGameEnd(raceResult);
            }
        }

        void IBotReceiver.EventYourCar(Id yourCar)
        {
            if (!ReferenceEquals(yourCar, null))
            {
                OnYourCar(yourCar);
            }
        }

        void IBotReceiver.EventJoin()
        {
            OnJoin();
        }

        void IBotReceiver.EventCarPositions(CarPositions carPositions)
        {
            if (!ReferenceEquals(carPositions, null))
            {
                OnCarPositions(carPositions);
            }
        }

        void IBotReceiver.EventTurboStart(TurboStart turboStart)
        {
            if (!ReferenceEquals(turboStart, null))
            {
                OnTurboStart(turboStart);
            }
        }

        void IBotReceiver.EventTurboEnd(TurboEnd turboEnd)
        {
            if (!ReferenceEquals(turboEnd, null))
            {
                OnTurboEnd(turboEnd);
            }
        }

        #endregion
    }
}
