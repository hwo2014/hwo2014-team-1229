﻿using System.Diagnostics;
using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class CarPhysics : IGameTickProvider
    {
        #region Static Fields

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public static readonly CarPhysics Empty = new CarPhysics();

        #endregion

        #region Methods

        public double GetPosition()
        {
            return IsCorrect ? Position : EstimatedPosition;
        }

        public double GetVelocity()
        {
            return IsCorrect ? Velocity : EstimatedVelocity;
        }

        public double GetAcceleration()
        {
            return IsCorrect ? Acceleration : EstimatedAcceleration;
        }

        public double GetThrottleAcceleration()
        {
            return IsCorrect ? ThrottleAcceleration : EstimatedThrottleAcceleration;
        }

        public double GetAngularAcceleration()
        {
            return IsCorrect ? AngularAcceleration : EstimatedAngularAcceleration;
        }

        public double GetAngularVelocity()
        {
            return IsCorrect ? AngularVelocity : EstimatedAngularVelocity;
        }

        public double GetAngle()
        {
            return IsCorrect ? Angle : EstimatedAngle;
        }

        #endregion

        #region Properties

        public int GameTick { get; set; }

        public double Position { get; set; }
        public double Velocity { get; set; }
        public double Acceleration { get; set; }
        public double Angle { get; set; }
        public double AngularVelocity { get; set; }
        public double AngularAcceleration { get; set; }

        public double EstimatedPosition { get; set; }
        public double EstimatedVelocity { get; set; }
        public double EstimatedAcceleration { get; set; }
        public double EstimatedAngle { get; set; }
        public double EstimatedAngularVelocity { get; set; }
        public double EstimatedAngularAcceleration { get; set; }


        public double Throttle { get; set; }

        public double ThrottleAcceleration { get; set; }
        public double EstimatedThrottleAcceleration { get; set; }

        public bool IsCorrect
        {
            get { return !ReferenceEquals(CarPosition, null); }
        }

        [CanBeNull]
        public CarPosition CarPosition { get; set; }

        public double ErrorThrottleAcceleration
        {
            get { return ThrottleAcceleration - EstimatedThrottleAcceleration; }
        }

        public double ErrorPosition
        {
            get { return Position - EstimatedPosition; }
        }

        public double ErrorVelocity
        {
            get { return Velocity - EstimatedVelocity; }
        }

        public double ErrorAcceleration
        {
            get { return Acceleration - EstimatedAcceleration; }
        }

        public double ErrorAngle
        {
            get { return Angle - EstimatedAngle; }
        }

        public double ErrorAngularVelocity
        {
            get { return AngularVelocity - EstimatedAngularVelocity; }
        }

        public double ErrorAngularAcceleration
        {
            get { return AngularAcceleration - EstimatedAngularAcceleration; }
        }

        #endregion
    }
}