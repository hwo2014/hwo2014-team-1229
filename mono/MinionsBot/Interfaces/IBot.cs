﻿using JetBrains.Annotations;
using Minions.Messages.JSON;

namespace Minions.Interfaces
{
    public interface IBot : IBotReceiver
    {
        [CanBeNull]
        string Key { get; }

        /// <summary>
        ///     Gets the name of the bot.
        /// </summary>
        [NotNull]
        string Name { get; }

        [NotNull]
        IBotProperties Properties { get; }

        [NotNull]
        IBotMemory Memory { get; }

        [NotNull]
        IBotPhysics Physics { get; }

        /// <summary>
        ///     Gets the current track.
        /// <returns>Null if the game has not been initialized yet.</returns>
        /// </summary>
        [CanBeNull]
        ITrack Track { get; }

        void Throttle(double value, int gameTick);

        void ActivateTurbo(int gameTick);

        void SendPing();

        void SwitchLeft(int gameTick);

        void SwitchRight(int gameTick);

        bool EqualId([CanBeNull] Id id);
    }
}
