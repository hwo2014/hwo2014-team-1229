﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Messages.JSON;

namespace Minions.Interfaces
{
    public interface IBotMemory : IBotReceiver
    {
        #region Properties

        [NotNull]
        IReadOnlyDictionary<string, IList<Spawn>> Spawns { get; }

        [NotNull]
        IReadOnlyDictionary<string, Finish> Finishes { get; }

        [NotNull]
        IReadOnlyDictionary<string, Disqualified> Disqualifications { get; }

        [NotNull]
        IReadOnlyDictionary<string, IList<Crash>> Crashes { get; }

        [NotNull]
        IReadOnlyDictionary<string, IList<LapFinished>> Laps { get; }

        [NotNull]
        IReadOnlyList<TurboData> Turbos { get; }

        [NotNull]
        IReadOnlyList<IReadOnlyList<CarPosition>> AllCarPositions { get; }

        [NotNull]
        IReadOnlyDictionary<string, SortedList<int, CarPosition>> CarPositions { get; }

        [NotNull]
        SortedList<int, double> Throttles { get; }
        
        [CanBeNull]
        Race Race { get; }

        [CanBeNull]
        RaceResult RaceResult { get; }

        #endregion

        #region Methods

        void AddThrottle(double throttle, int gameTick);

        void Reset();

        #endregion
    }
}
