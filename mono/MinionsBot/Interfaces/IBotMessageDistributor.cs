﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Messages.JSON;

namespace Minions.Interfaces
{
    public interface IBotMessageDistributor
    {
        #region Properties

        [NotNull]
        IReadOnlyList<IBotReceiver> Receivers { get; } 

        #endregion

        #region Methods

        void AddReceiver([CanBeNull] IBotReceiver receiver);

        bool RemoveReceiver([CanBeNull] IBotReceiver receiver);

        void Clear();

        void AddEventTurboAvailable([CanBeNull] TurboAvailable data);

        void AddEventFinish([CanBeNull] Finish data);

        void AddEventDisqualified([CanBeNull] Disqualified data);

        void AddEventLapFinished([CanBeNull] LapFinished data);

        void AddEventSpawn([CanBeNull] Spawn data);

        void AddEventTournamentEnd([CanBeNull] TournamentEnd data);

        void AddEventCrash([CanBeNull] Crash data);

        void AddEventYourCar([CanBeNull] Id data);

        void AddEventGameStart();

        void AddEventGameEnd([CanBeNull] RaceResult data);

        void AddEventGameInit([CanBeNull] RaceInit data);

        void AddEventJoin();

        void AddEventCarPositions([CanBeNull] CarPositions data);

        void AddEventTurboStart([CanBeNull] TurboStart data);

        void AddEventTurboEnd([CanBeNull] TurboEnd data);

        #endregion
    }
}
