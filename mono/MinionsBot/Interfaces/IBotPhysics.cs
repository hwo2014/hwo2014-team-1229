﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Minions.Interfaces
{
    public interface IBotPhysics : IBotProvider
    {
        #region Properties

        [NotNull]
        string Name { get; }

        int Count { get; }

        #endregion

        #region Methods

        void Reset();

        void LearnThrottleFactor(int gameTick, double throttle);

        bool UpdateIntegration(int gameTick, int timeStep);

        /// <summary>
        ///     Computes the next physic values of the car.
        /// </summary>
        /// <param name="gameTick">The current game time.</param>
        /// <param name="timeStep">The simulation time step.</param>
        /// <param name="throttle">The throttle value</param>
        [NotNull]
        CarPhysics Integrate(int gameTick, int timeStep, double throttle);
        
        [NotNull]
        CarPhysics IntegrateAdd(int gameTick, int timeStep, double throttle);

        bool TryGetCarPhysics(int gameTick, int timeStep, out CarPhysics carPhysics);

        bool TryGetCarPhysics(int gameTick, out CarPhysics carPhysics);

        [NotNull]
        SortedList<int, CarPhysics> Simulate(int gameTick, int timeStep, [NotNull] double[] throttles);

        #endregion
    }
}
