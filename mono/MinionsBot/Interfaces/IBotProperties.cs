﻿using Minions.Messages.JSON;

namespace Minions.Interfaces
{
    public interface IBotProperties : IBotReceiver
    {
        #region Properties

        int GameTick { get; }
        TurboData AvailableTurbo { get; }
        bool IsTurboAvailable { get; }
        int Lap { get; }
        int LastLapMilliseconds { get; }
        bool IsDisqualified { get; }
        bool IsRaceStarted { get; }
        bool IsRaceFinished { get; }
        bool IsGameEnded { get; }
        bool IsTournamentEnded { get; }
        bool IsJoined { get; }
        bool IsCrashed { get; }
        bool IsGameInitialized { get; }

        int TicksSinceStartOrSpawn { get; }

        #endregion

        #region Methods

        void Reset();

        #endregion
    }
}
