﻿using JetBrains.Annotations;

namespace Minions.Interfaces
{
    public interface IBotProvider
    {
        #region Properties

        [NotNull]
        IBot Bot { get; }

        #endregion
    }
}
