﻿using JetBrains.Annotations;
using Minions.Messages.JSON;

namespace Minions.Interfaces
{
    public interface IBotReceiver : IBotProvider
    {
        #region Methods

        void EventTurboAvailable([CanBeNull] TurboAvailable turboAvailable);
        void EventSpawn([CanBeNull] Spawn spawn);
        void EventFinish([CanBeNull] Finish finish);
        void EventDisqualified([CanBeNull] Disqualified dnf);
        void EventCrash([CanBeNull] Crash crash);
        void EventTournamentEnd([CanBeNull] TournamentEnd tournamentEnd);
        void EventLapFinished([CanBeNull] LapFinished lapFinished);
        void EventGameInit([CanBeNull] RaceInit raceInit);
        void EventGameStart();
        void EventGameEnd([CanBeNull] RaceResult raceResult);
        void EventYourCar([CanBeNull] Id yourCar);
        void EventJoin();
        void EventCarPositions([CanBeNull] CarPositions carPositions);
        void EventTurboStart([CanBeNull] TurboStart turboStart);
        void EventTurboEnd([CanBeNull] TurboEnd turboEnd);

        #endregion
    }
}
