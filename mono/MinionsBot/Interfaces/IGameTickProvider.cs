﻿namespace Minions.Interfaces
{
    public interface IGameTickProvider
    {
        int GameTick { get; }
    }
}
