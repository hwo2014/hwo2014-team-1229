﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Messages.JSON;

namespace Minions.Interfaces
{
    public interface ITrack
    {
        #region Properties

        /// <summary>
        ///     Gets a list of pieces which build up the track.
        /// </summary>
        [NotNull]
        IReadOnlyList<Piece> Pieces { get; }

        /// <summary>
        ///     Gets a list of available lanes.
        /// </summary>
        [NotNull]
        IReadOnlyList<Lane> Lanes { get; }

        /// <summary>
        ///     Gets the starting point of the track.
        /// </summary>
        [NotNull]
        StartingPoint StartingPoint { get; }

        /// <summary>
        ///     Gets the id of the track.
        /// </summary>
        [NotNull]
        string Id { get; }

        /// <summary>
        ///     Gets the name of the track.
        /// </summary>
        [NotNull]
        string Name { get; }

        /// <summary>
        ///     Gets the number of laps.
        /// </summary>
        int LapCount { get; }
        
        #endregion

        #region Methods

        /// <summary>
        ///     Checks whether the given <paramref name="piece"/> is a curve.
        /// </summary>
        /// <param name="piece"></param>
        /// <returns>True if curve, false otherwise.</returns>
        bool IsCurve([NotNull] Piece piece);

        /// <summary>
        ///     Checks whether the piece given by <paramref name="pieceIndex"/> is a curve.
        /// </summary>
        /// <param name="pieceIndex"></param>
        /// <returns>True if curve, false otherwise.</returns>
        bool IsCurve(int pieceIndex);

        /// <summary>
        ///     Checks whether the given <paramref name="piece"/> is a line.
        /// </summary>
        /// <param name="piece"></param>
        /// <returns>True if line, false otherwise.</returns>
        bool IsLine([NotNull] Piece piece);

        /// <summary>
        ///     Checks whether the piece given by <paramref name="pieceIndex"/> is a line.
        /// </summary>
        /// <param name="pieceIndex"></param>
        /// <returns>True if line, false otherwise.</returns>
        bool IsLine(int pieceIndex);

        /// <summary>
        ///     Checks whether the pieces between <paramref name="startPieceIndex"/> and <paramref name="endPieceIndex"/> form a line.
        /// </summary>
        /// <param name="startPieceIndex"></param>
        /// <param name="endPieceIndex"></param>
        /// <returns>True if line, false otherwise.</returns>
        bool IsLine(int startPieceIndex, int endPieceIndex);

        /// <summary>
        ///     Checks whether the given <paramref name="piecePosition"/> is on a curve.
        /// </summary>
        /// <param name="piecePosition"></param>
        /// <returns>True if the given <paramref name="piecePosition"/> is on a curve, false otherwise.</returns>
        bool IsOnCurve([NotNull] PiecePosition piecePosition);

        /// <summary>
        ///     Checks whether the given <paramref name="piecePosition"/> is on a line.
        /// </summary>
        /// <param name="piecePosition"></param>
        /// <returns>True if the given <paramref name="piecePosition"/> is on a line, false otherwise.</returns>
        bool IsOnLine([NotNull] PiecePosition piecePosition);

        /// <summary>
        ///     Gets the pieces which form a straight line, starting at <paramref name="piece"/>.
        /// </summary>
        /// <param name="piece"></param>
        /// <returns>Null if the given <paramref name="piece"/> is not a line, otherwise a ordered list of pieces that form the line.</returns>
        [CanBeNull]
        IReadOnlyList<Piece> GetLine([NotNull] Piece piece);

        IReadOnlyList<Piece> GetLine(int pieceIndex);

        IReadOnlyList<Piece> GetCurve(int pieceIndex);

        int GetLineLength(int pieceIndex);

        int GetPieceDistance(int pieceIndex1, int pieceIndex2);

        /// <summary>
        ///     Gets the index of the last piece of a straight line which is starting at the piece given by <paramref name="startPieceIndex"/>.
        /// </summary>
        /// <param name="startPieceIndex"></param>
        /// <returns>Zero if the piece specified by <paramref name="startPieceIndex"/> is not a line, otherwise the index of the last piece of the line.</returns>
        int GetEndOfLineIndex(int startPieceIndex);

        /// <summary>
        ///     Measures the distance to the start/finish line starting from <paramref name="piecePosition"/>.
        /// </summary>
        /// <param name="piecePosition">The starting position.</param>
        /// <returns>The distance.</returns>
        double MeasureDistanceToFinishLine([NotNull] PiecePosition piecePosition);

        /// <summary>
        ///     Measures the distance from the start line to <paramref name="piecePosition"/> (using the center of the track).
        /// </summary>
        /// <param name="piecePosition"></param>
        /// <returns>The distance.</returns>
        double MeasureDistanceFromStartLine([NotNull] PiecePosition piecePosition);

        /// <summary>
        ///     Measures the total length of this track.
        /// </summary>
        /// <returns>The length of this track.</returns>
        double MeasureTotalLength();
        
        /// <summary>
        ///     Moves the car <paramref name="distance"/> units starting from <paramref name="piecePosition"/>.
        /// </summary>
        /// <param name="piecePosition">The starting position</param>
        /// <param name="distance">The distance to move</param>
        [NotNull]
        PiecePosition MoveTo([NotNull] PiecePosition piecePosition, double distance);

        /// <summary>
        ///     Measures the central length of the given <paramref name="piece"/>.
        /// </summary>
        /// <param name="piece"></param>
        /// <returns>The length of <paramref name="piece"/>.</returns>
        double MeasureLength([NotNull] Piece piece);

        double MeasureLength(int pieceIndex);

        /// <summary>
        ///     Measures the length of the given <paramref name="piece"/> using <paramref name="lane"/>.
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="lane"></param>
        /// <returns>The length of <paramref name="piece"/>.</returns>
        double MeasureLength([NotNull] Piece piece, Lane lane);

        double MeasureLength(int pieceIndex, int laneIndex);

        double MeasureRemainingLength([NotNull] PiecePosition piecePosition);

        double GetCurveAngle(int pieceIndex);

        #endregion
    }
}
