using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Car
    {
        [JsonProperty("id")]
        public Id Id { get; set; }

        [JsonProperty("dimensions")]
        public Dimensions Dimensions { get; set; }
    }
}