﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    /// <summary>
    /// describes the position a car on the track.
    /// </summary>
    public sealed class CarPosition
    {
        [JsonProperty("id")]
        public Id Id { get; set; }

        [JsonProperty("angle")]
        public double Angle { get; set; }

        [JsonProperty("piecePosition")]
        public PiecePosition PiecePosition { get; set; }

        [JsonProperty("lap")]
        public int Lap { get; set; }
    }
}