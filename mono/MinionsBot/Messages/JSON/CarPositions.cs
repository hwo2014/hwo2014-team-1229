﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class CarPositions : ReceiveMessage
    {
        public const string MsgId = "carPositions";

        [JsonProperty("data")]
        public List<CarPosition> Positions { get; set; }

        protected override object MsgData()
        {
            return Positions;
        }

        protected override string MsgType()
        {
            return MsgId;
        }
    }
}
