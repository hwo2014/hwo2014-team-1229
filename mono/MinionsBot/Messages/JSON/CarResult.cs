﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class CarResult
    {
        [JsonProperty("car")]
        public Id CarId { get; set; }

        [JsonProperty("result")]
        public Result Result { get; set; }
    }
}