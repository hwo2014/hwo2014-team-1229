﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Crash : ReceiveMessage
    {
        public const string MsgId = "crash";

        [JsonProperty("data")]
        public Id CarId { get; set; }

        protected override object MsgData()
        {
            return CarId;
        }

        protected override string MsgType()
        {
            return MsgId;
        }
    }
}
