using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Dimensions
    {
        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("width")]
        public double Width { get; set; }

        [JsonProperty("guideFlagPosition")]
        public double GuideFlagPosition { get; set; }
    }
}