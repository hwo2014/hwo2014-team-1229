﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class DisqualifiedData
    {
        [JsonProperty("car")]
        public Id CarId { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }
    }

    public sealed class Disqualified : ReceiveMessage
    {
        public const string MsgId = "dnf";

        [JsonProperty("data")]
        public DisqualifiedData Data { get; set; }

        protected override string MsgType()
        {
            return MsgId;
        }

        protected override object MsgData()
        {
            return Data;
        }
    }
}
