﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Finish : ReceiveMessage
    {
        public const string MsgId = "finish";

        [JsonProperty("data")]
        public Id CarId { get; set; }

        protected override string MsgType()
        {
            return MsgId;
        }

        protected override object MsgData()
        {
            return CarId;
        }
    }
}
