using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Id
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }
}