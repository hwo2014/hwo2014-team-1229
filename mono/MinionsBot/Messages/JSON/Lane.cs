using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Lane
    {
        [JsonProperty("distanceFromCenter")]
        public int DistanceFromCenter { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }
    }
}