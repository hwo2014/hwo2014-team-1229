﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class LaneIndex
    {
        [JsonProperty("startLaneIndex")]
        public int StartLaneIndex { get; set; }

        [JsonProperty("endLaneIndex")]
        public int EndLaneIndex { get; set; }
    }
}