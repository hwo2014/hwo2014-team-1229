﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class LapFinishedData
    {
        [JsonProperty("car")]
        public Id CarId { get; set; }

        [JsonProperty("lapTime")]
        public Time LapTime { get; set; }

        [JsonProperty("raceTime")]
        public Time RaceTime { get; set; }

        [JsonProperty("ranking")]
        public Ranking Ranking { get; set; }
    }

    public sealed class LapFinished : ReceiveMessage
    {
        public const string MsgId = "lapFinished";

        [JsonProperty("data")]
        public LapFinishedData Data { get; set; }

        protected override object MsgData()
        {
            return Data;
        }
        
        protected override string MsgType()
        {
            return MsgId;
        }
    }
}
