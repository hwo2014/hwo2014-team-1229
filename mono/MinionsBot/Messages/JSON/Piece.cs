using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Piece
    {
        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("switch")]
        public bool? Switch { get; set; }

        [JsonProperty("radius")]
        public int? Radius { get; set; }

        [JsonProperty("angle")]
        public double? Angle { get; set; }
    }
}