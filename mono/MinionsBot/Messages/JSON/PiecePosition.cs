using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class PiecePosition
    {
        /// <summary>
        /// zero based index of the piece the car is on
        /// </summary>
        [JsonProperty("pieceIndex")]
        public int PieceIndex { get; set; }

        /// <summary>
        /// the distance the car's Guide Flag (see above) has traveled from the start of the piece along the current lane
        /// </summary>
        [JsonProperty("inPieceDistance")]
        public double InPieceDistance { get; set; }

        //a pair of lane indices. Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car is currently switching lane
        [JsonProperty("lane")]
        public LaneIndex Lane { get; set; }

        /// <summary>
        /// the number of laps the car has completed. The number 0 indicates that the car is on its first lap. The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
        /// </summary>
        [JsonProperty("lap")]
        public int Lap { get; set; }
    }
}