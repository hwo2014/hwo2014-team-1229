using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Position
    {
        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }
    }
}