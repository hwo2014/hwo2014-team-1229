using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Race
    {
        [JsonProperty("track")]
        public Track Track { get; set; }

        [JsonProperty("cars")]
        public List<Car> Cars { get; set; }

        [JsonProperty("raceSession")]
        public RaceSession RaceSession { get; set; }
    }
}