using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class RaceInit : SendMessage
    {
        public const string MsgId = "gameInit";

        [JsonProperty("race")]
        public Race Race { get; set; }

        protected override string MsgType()
        {
            return MsgId;
        }
    }
}