﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class RaceResult
    {
        [JsonProperty("results")]
        public List<CarResult> Results { get; set; }

        [JsonProperty("bestLaps")]
        public List<CarResult> BestLaps { get; set; }
    }
}