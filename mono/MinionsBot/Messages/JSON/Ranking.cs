﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Ranking
    {
        [JsonProperty("overall")]
        public int Overall { get; set; }

        [JsonProperty("fastestLap")]
        public int FastestLap { get; set; }
    }
}
