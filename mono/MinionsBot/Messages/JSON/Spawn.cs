﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Spawn : ReceiveMessage
    {
        public const string MsgId = "spawn";

        [JsonProperty("data")]
        public Id CarId { get; set; }

        protected override string MsgType()
        {
            return MsgId;
        }

        protected override object MsgData()
        {
            return CarId;
        }
    }
}
