using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class StartingPoint
    {
        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("angle")]
        public double Angle { get; set; }
    }
}