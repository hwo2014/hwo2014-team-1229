﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Time
    {
        [JsonProperty("lap")]
        public int Lap { get; set; }

        [JsonProperty("ticks")]
        public int Ticks { get; set; }

        [JsonProperty("millis")]
        public int Millis { get; set; }
    }
}
