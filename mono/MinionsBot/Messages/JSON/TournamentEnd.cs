﻿namespace Minions.Messages.JSON
{
    public sealed class TournamentEnd : SendMessage
    {
        public const string MsgId = "tournamentEnd";

        protected override string MsgType()
        {
            return MsgId;
        }
        
        protected override object MsgData()
        {
            return null;
        }
    }
}
