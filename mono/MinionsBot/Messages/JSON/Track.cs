using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class Track
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pieces")]
        public List<Piece> Pieces { get; set; }

        [JsonProperty("lanes")]
        public List<Lane> Lanes { get; set; }

        [JsonProperty("startingPoint")]
        public StartingPoint StartingPoint { get; set; }
    }
}