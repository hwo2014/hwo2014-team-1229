﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class TurboData
    {
        [JsonProperty("turboDurationMilliseconds")]
        public double DurationMilliseconds { get; set; }

        [JsonProperty("turboDurationTicks")]
        public int DurationTicks { get; set; }

        [JsonProperty("turboFactor")]
        public double Factor { get; set; }
    }

    public sealed class TurboAvailable : SendMessage
    {
        public const string MsgId = "turboAvailable";

        [JsonProperty("data")]
        public TurboData Turbo { get; set; }

        protected override object MsgData()
        {
            return Turbo;
        }

        protected override string MsgType()
        {
            return MsgId;
        }
    }
}
