﻿using Newtonsoft.Json;

namespace Minions.Messages.JSON
{
    public sealed class TurboEnd : ReceiveMessage
    {
        public const string MsgId = "turboEnd";

        [JsonProperty("data")]
        public Id CarId { get; set; }

        #region Overrides of SendMessage

        protected override object MsgData()
        {
            return CarId;
        }

        protected override string MsgType()
        {
            return MsgId;
        }

        #endregion
    }
}
