using Newtonsoft.Json;

namespace Minions.Messages
{
    public sealed class Join : SendMessage
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("Color")]
        public string Color { get; set; }

        public const string MsgId = "join";

        public Join(string name, string key, string color = "red")
        {
            Name = name;
            Key = key;
            Color = color;
        }

        protected override string MsgType()
        {
            return MsgId;
        }
    }
}