namespace Minions.Messages
{
    public sealed class Ping : SendMessage
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}