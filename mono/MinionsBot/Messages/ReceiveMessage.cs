﻿using Minions.Interfaces;
using Newtonsoft.Json;

namespace Minions.Messages
{
    public abstract class ReceiveMessage : SendMessage, IGameTickProvider
    {
        [JsonProperty("gameId")]
        public string GameId { get; set; }

        [JsonProperty("gameTick")]
        public int GameTick { get; set; }

        public new string ToJson()
        {
            return JsonConvert.SerializeObject(new ReceiveMessageWrapper(MsgType(), MsgData(), GameId, GameTick));
        }
    }
}
