﻿using Newtonsoft.Json;

namespace Minions.Messages
{
    public sealed class ReceiveMessageWrapper : SendMessageWrapper
    {
        #region Properties

        [JsonProperty("gameId")]
        public string GameId { get; set; }

        [JsonProperty("gameTick")]
        public int GameTick { get; set; }

        #endregion

        #region Constructor

        public ReceiveMessageWrapper(string msgType, object data, string gameId, int gameTick) 
            : base(msgType, data)
        {
            GameId = gameId;
            GameTick = gameTick;
        }

        #endregion
    }
}
