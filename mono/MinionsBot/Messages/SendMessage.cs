using System;
using Newtonsoft.Json;

namespace Minions.Messages
{
    public abstract class SendMessage
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new SendMessageWrapper(MsgType(), MsgData()));
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}