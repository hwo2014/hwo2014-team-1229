﻿using Newtonsoft.Json;

namespace Minions.Messages
{
    public class SendMessageWrapper
    {
        #region Properties

        [JsonProperty("msgType")]
        public string MsgType { get; set; }

        [JsonProperty("data")]
        public object Data { get; set; }

        #endregion

        #region Constructor

        public SendMessageWrapper(string msgType, object data)
        {
            MsgType = msgType;
            Data = data;
        }

        #endregion
    }
}
