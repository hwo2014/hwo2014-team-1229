﻿namespace Minions.Messages
{
    public abstract class SwitchLane : SendMessage
    {
        #region Constants

        public string Left = "Left";
        public string Right = "Right";

        #endregion

        protected override string MsgType()
        {
            return "switchLane";
        }
    }

    public sealed class SwitchLaneLeft : SwitchLane
    {
        protected override object MsgData()
        {
            return Left;
        }
    }

    public sealed class SwitchLaneRight : SwitchLane
    {
        protected override object MsgData()
        {
            return Right;
        }
    }
}
