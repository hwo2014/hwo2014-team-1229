using System;

namespace Minions.Messages
{
    public sealed class Throttle : SendMessage
    {
        public double value { get; set; }

        public int gameTick { get; set; }

        public Throttle(double value)
        {
            this.value = Math.Min(1.0, Math.Max(0.0, value));
        }

        protected override Object MsgData()
        {
            return value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}