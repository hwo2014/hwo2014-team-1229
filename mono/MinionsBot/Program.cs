﻿using System;
using System.IO;
using System.Net.Sockets;
using Minions.Messages;

namespace Minions
{
    public static class Program
    {
        #region Entry Point

        public static void Main(string[] args)
        {
            var host = args[0];
            var port = int.Parse(args[1]);
            var botName = args[2];
            var botKey = args[3];

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (var client = new TcpClient(host, port))
            {
                var stream = client.GetStream();
                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream) {AutoFlush = true};

                var bot = new Bot(reader, writer, new Join(botName, botKey));
                bot.GameLoop();
            }
        }

        #endregion
    }
}
