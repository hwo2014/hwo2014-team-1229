﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Minions.Interfaces;
using Minions.Messages.JSON;

namespace Minions
{
    public sealed class TrackManager : ITrack
    {
        //number of cars in this race
        int NumberOfCars;
        //Id SelfId;

        //lanes on this track
        int NumberOfLanes;

        //all cars
        List<Car> cars;

        //all pieces of this track
        public  List<Piece> Track;
        private double[] Speed;

        //pro piece speichern:
        //angle,
        //throttle
        //crashed?

        public TrackManager([NotNull] Race race)
        {
            Pieces = race.Track.Pieces;
            Lanes = race.Track.Lanes;
            LapCount = race.RaceSession.Laps;
            Name = race.Track.Name;
            Id = race.Track.Id;
            StartingPoint = race.Track.StartingPoint;

            cars = race.Cars;
            NumberOfLanes = race.Track.Lanes.Count;
            NumberOfCars = cars.Count;


            Track = race.Track.Pieces;
            //Console.WriteLine("trackLength: " + CalculateTrackLength());
            //add length to the curves
            PrepareTrack(race.Track.Pieces);
            //Console.WriteLine("trackLength2: " + CalculateTrackLength());

            Speed = new double[Track.Count];

            //initiate speed list
            for (int i = 0; i < Speed.Length; i++)
            {
                Speed[i] = 0.7;
            }
            

        }

        public void ReportCrash(int piece, double inPieceDist)
        {
            if (--piece < 0)
            {
                piece = Speed.Length - 1;
            }

            Speed[piece] -= 0.1;
            Console.WriteLine("throttle now: " + Speed[piece]);
        }

        public double GetThrottleForTrack(int piece, double inPieceDist)
        {
            return Speed[piece];
        }

        /// <summary>
        /// calculate some misc data and add it to the track info
        /// adds length to bend pieces
        /// </summary>
        /// <param name="trackData"></param>
        private void PrepareTrack(List<Piece> trackData)
        {
            //calculate length of bend pieces
            for (int i = 0; i < Track.Count; i++)
            {
                //length of a bend is = radius * pi * angle/180
                if (Track[i].Angle != null)
                {
                    
                    double arc = (Track[i].Angle.HasValue) ? (double)(Track[i].Angle / 180.0) : 0;
                    //make arc positive, because lenght!
                    if (arc < 0) arc = arc * -1;

                    Track[i].Length = (double)Track[i].Radius * Math.PI * arc;

                }
            }
        }

        private double CalculateTrackLength()
        {
            double length = 0.0;
            for (int i = 0; i < Track.Count; i++)
            {
                length += Track[i].Length;
            }

            return length;
        }

        #region Implementation of ITrack

        public IReadOnlyList<Piece> Pieces { get; private set; }

        public IReadOnlyList<Lane> Lanes { get; private set; }

        public StartingPoint StartingPoint { get; private set; }

        public string Id { get; private set; }

        public string Name { get; private set; }

        public int LapCount { get; private set; }

        public bool IsCurve(Piece piece)
        {
            return !IsLine(piece);
        }

        public bool IsCurve(int pieceIndex)
        {
            return IsCurve(Pieces[pieceIndex]);
        }

        public bool IsLine(Piece piece)
        {
            return !piece.Angle.HasValue;
        }

        public bool IsLine(int pieceIndex)
        {
            return IsLine(Pieces[pieceIndex]);
        }

        public bool IsLine(int startPieceIndex, int endPieceIndex)
        {
            var index = startPieceIndex;
            while (IsLine(index))
            {
                ++index;
                if (index >= endPieceIndex)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsOnCurve(PiecePosition piecePosition)
        {
            return IsCurve(piecePosition.PieceIndex);
        }

        public bool IsOnLine(PiecePosition piecePosition)
        {
            return IsLine(piecePosition.PieceIndex);
        }

        public IReadOnlyList<Piece> GetLine(Piece piece)
        {
            throw new NotImplementedException();
        }

        private IReadOnlyList<Piece> GetXYZ(int pieceIndex, Func<Piece, bool> predicate)
        {
            var pieces = new List<Piece>(0);
            var index = pieceIndex;
            while (predicate(Pieces[index]))
            {
                pieces.Add(Pieces[index]);
                ++index;
                if (index == pieceIndex)
                {
                    break;
                }
                if (index >= Pieces.Count)
                {
                    index = 0;
                }
            }
            return pieces;
        }

        public IReadOnlyList<Piece> GetLine(int pieceIndex)
        {
            return GetXYZ(pieceIndex, IsLine);
        }

        public IReadOnlyList<Piece> GetCurve(int pieceIndex)
        {
            return GetXYZ(pieceIndex, IsCurve);
        }

        public int GetLineLength(int pieceIndex)
        {
            var length = 0;
            var index = pieceIndex;
            while (IsLine(index))
            {
                ++length;
                ++index;
                if (index == pieceIndex)
                {
                    break;
                }
                if (index >= Pieces.Count)
                {
                    index = 0;
                }
            }
            return length;
        }

        public int GetPieceDistance(int pieceIndex1, int pieceIndex2)
        {
            if (pieceIndex1 <= pieceIndex2)
            {
                return Math.Min(pieceIndex2 - pieceIndex1, pieceIndex1 + Pieces.Count - pieceIndex2);
            }

            return -Math.Min(pieceIndex1 - pieceIndex2, pieceIndex2 + Pieces.Count - pieceIndex1);
        }

        public int GetEndOfLineIndex(int startPieceIndex)
        {
            var index = startPieceIndex;
            while (IsLine(index))
            {
                ++index;
                if (index >= Pieces.Count)
                {
                    index = 0;
                }
                else if (index == startPieceIndex)
                {
                    return index - 1;
                }
            }
            return index;
        }

        public double MeasureDistanceToFinishLine(PiecePosition piecePosition)
        {
            throw new NotImplementedException();
        }

        public double MeasureDistanceFromStartLine(PiecePosition piecePosition)
        {
            throw new NotImplementedException();
        }

        public double MeasureTotalLength()
        {
            throw new NotImplementedException();
        }

        public PiecePosition MoveTo(PiecePosition piecePosition, double distance)
        {
            throw new NotImplementedException();
        }

        public double MeasureLength(Piece piece)
        {
            throw new NotImplementedException();
        }

        public double MeasureLength(int pieceIndex)
        {
            return MeasureLength(Pieces[pieceIndex]);
        }

        public double MeasureLength(Piece piece, Lane lane)
        {
            // TODO correct implementation
            if (piece.Length > 0)
            {
                return piece.Length;
            }

            if (piece.Radius.HasValue && piece.Angle.HasValue)
            {
                return Math.PI*(piece.Radius.Value + lane.DistanceFromCenter)*Math.Abs(piece.Angle.Value)/180.0;
            }

            return 0.0;
        }

        public double MeasureLength(int pieceIndex, int laneIndex)
        {
            return MeasureLength(Pieces[pieceIndex], Lanes[laneIndex]);
        }

        public double MeasureRemainingLength(PiecePosition piecePosition)
        {
            var length = MeasureLength(piecePosition.PieceIndex, piecePosition.Lane.EndLaneIndex);
            return length - piecePosition.InPieceDistance;
        }

        public double GetCurveAngle(int pieceIndex)
        {
            pieceIndex %= Pieces.Count;

            var curve = GetCurve(pieceIndex);
            if (curve.Count <= 0)
            {
                return 0.0;
            }

            if (!curve[0].Angle.HasValue)
            {
                return 0.0;
            }

            var angle = curve[0].Angle.Value;
            var isNegative = curve[0].Angle.Value < 0.0;

            for (var index = 1; index < curve.Count; ++index)
            {
                var piece = curve[index];
                if (!piece.Angle.HasValue)
                {
                    break;
                }

                var pAngle = piece.Angle.Value;
                if ((pAngle < 0.0 && isNegative) || (pAngle >= 0.0 && !isNegative))
                {
                    angle += pAngle;
                }
                else
                {
                    break;
                }
            }

            return angle;
        }

        #endregion
    }
}
